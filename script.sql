--- Suppresion des sequences  ---

DROP SEQUENCE IF EXISTS seq_commentaire;
DROP SEQUENCE IF EXISTS seq_bloc;


--- Supression des tables existantes ---

DROP TABLE IF EXISTS Utilisateur CASCADE;
DROP TABLE IF EXISTS Redacteur CASCADE;
DROP TABLE IF EXISTS Commentaire CASCADE;
DROP TABLE IF EXISTS Administrateur CASCADE;
DROP TABLE IF EXISTS Modérateur CASCADE;
DROP TABLE IF EXISTS Lecteur CASCADE;
DROP TABLE IF EXISTS Editeur CASCADE ;
DROP TABLE IF EXISTS Comite_editorial CASCADE;
DROP TABLE IF EXISTS Article CASCADE;
DROP TABLE IF EXISTS Mots_cles CASCADE;
DROP TABLE IF EXISTS Statut_article CASCADE;
DROP TABLE IF EXISTS Bloc CASCADE;
DROP TABLE IF EXISTS Rubrique CASCADE;
DROP TABLE IF EXISTS Article_Rubrique CASCADE;
DROP TABLE IF EXISTS Rubrique_Index CASCADE;
DROP TABLE IF EXISTS Commentaire_Moderateur CASCADE;


DROP TYPE IF EXISTS statut_comm;
DROP TYPE IF EXISTS type_bloc;

--- Utilisateur ---

CREATE TABLE Utilisateur (
    mail varchar(255) PRIMARY KEY,
    nom varchar(255) NOT NULL,
    prenom varchar(255) not  NULL,
    telephone varchar(100) default NULL,
    date_naissance Date NOT NULL,
    derniere_connexion Date NOT NULL,
    date_creation Date NOT NULL
);


INSERT INTO Utilisateur (nom,prenom,telephone,mail,date_naissance,derniere_connexion,date_creation) VALUES
 ('Joel','Léonie','+33 2 12 69 58 14','dis@sitamet.co.uk','1985-10-05 13:55:23','2020-06-05 01:16:59','2009-04-20 22:59:47'),
 ('Abbot','Loevan','+33 8 75 61 74 36','nibh@Phaselluslibero.co.uk','1966-09-28 18:40:37','2020-10-16 03:45:48','2009-08-23 09:41:20'),
 ('Cairo','Benjamin','+33 6 37 58 05 66','rhoncus.Donec@ipsumSuspendissenon.net','1942-05-14 19:52:52','2020-06-01 14:46:02','2017-09-15 21:07:58'),
 ('Bert','Paul','+33 4 13 29 07 65','vulputate@tortordictumeu.ca','1999-09-27 22:14:59','2019-07-16 04:47:07','2015-07-04 20:17:21'),
 ('Jelani','Léa','+33 5 12 42 17 16','Duis@duiFusce.edu','1988-04-07 10:36:06','2021-01-04 06:44:03','2006-04-20 15:40:51'),
 ('Solomon','Jade','+33 3 15 29 62 60','turpis.egestas.Fusce@velitdui.org','1951-04-20 21:06:37','2021-04-29 17:50:30','2009-12-27 09:07:36'),
 ('Walker','Noémie','+33 9 81 63 92 49','at.fringilla.purus@ornarelectusjusto.co.uk','1997-06-24 20:38:50','2021-01-17 03:16:10','2015-03-31 11:14:24'),
 ('Declan','Nicolas','+33 2 00 83 50 55','sem.semper@condimentumeget.co.uk','1950-05-12 16:53:23','2020-09-24 15:50:07','2019-03-14 22:06:39'),
 ('Asher','Maxime','+33 5 75 12 20 75','sed.pede.Cum@imperdiet.ca','1954-05-21 06:51:25','2020-06-02 11:03:15','2013-01-06 07:05:14'),
 ('Holmes','Alexia','+33 5 53 89 82 93','orci@vehiculaPellentesquetincidunt.net','1967-01-20 16:40:58','2020-01-25 18:22:04','2010-01-25 21:35:15');



--- Redacteur ---

CREATE TABLE Redacteur (
  redacteur varchar(255) PRIMARY KEY,
  FOREIGN KEY (redacteur) REFERENCES Utilisateur(mail)
);

INSERT INTO Redacteur (redacteur) VALUES
('dis@sitamet.co.uk'),
('orci@vehiculaPellentesquetincidunt.net');



--- Comite_editorial ---

CREATE TABLE Comite_editorial (
  reunion Date PRIMARY KEY,
  prochaine_reunion Date NOT NULL,
  nb_member integer,
  ordre_du_jour varchar(255)
);

INSERT INTO Comite_editorial (reunion, prochaine_reunion, nb_member, ordre_du_jour) VALUES
('2021-05-22','2021-06-22',2,'Validation de article COVID21 et ajout du statut soumis');




--- Editeur ---

CREATE TABLE Editeur (
    editeur varchar(255) PRIMARY KEY,
    FOREIGN KEY (editeur) REFERENCES Utilisateur(mail),
    membres Date,
    FOREIGN KEY (membres) REFERENCES Comite_editorial(reunion)
);

INSERT INTO Editeur (editeur, membres) VALUES
('nibh@Phaselluslibero.co.uk','2021-05-22'),
('rhoncus.Donec@ipsumSuspendissenon.net', '2021-05-22');



--- Statut_article ---

CREATE TABLE Statut_article (
    nom varchar(255) PRIMARY KEY,
    comite_editorial Date NOT NULL,
    FOREIGN KEY (comite_editorial) REFERENCES Comite_editorial(reunion)
);

INSERT INTO Statut_article (nom, comite_editorial) VALUES
('soumis','2021-05-22'),
('validé','2021-05-22');



--- Lecteur ---

CREATE TABLE Lecteur (
    lecteur varchar(255) PRIMARY KEY,
    FOREIGN KEY (lecteur) REFERENCES Utilisateur(mail)
);

INSERT INTO Lecteur (lecteur) VALUES
('sed.pede.Cum@imperdiet.ca'),
('sem.semper@condimentumeget.co.uk'),
('Duis@duiFusce.edu');



--- Mots_cles ---

CREATE TABLE Mots_cles (
  nom varchar(255) PRIMARY KEY,
  date_creation varchar(255),
  nb_lecture integer,
  visiteur varchar(255),
  FOREIGN KEY (visiteur) REFERENCES Lecteur(lecteur),
  createur varchar(255),
  FOREIGN KEY (createur) REFERENCES Editeur(editeur)
);

INSERT INTO Mots_cles (nom, date_creation, nb_lecture, visiteur, createur) VALUES
('santé','2021-03-09','9999','sed.pede.Cum@imperdiet.ca','nibh@Phaselluslibero.co.uk'),
('sport','2021-05-09','1678','sem.semper@condimentumeget.co.uk','rhoncus.Donec@ipsumSuspendissenon.net');



--- Article ---

CREATE TABLE Article (
  nom varchar(255) PRIMARY KEY,
  date_creation Date NOT NULL,
  derniere_modif Date NOT NULL,
  nb_vue integer,
  nb_acces_via_article_lie integer,
  auteur varchar(255),
  FOREIGN KEY (auteur) REFERENCES Redacteur(redacteur),
  tag varchar(255),
  FOREIGN KEY (tag) REFERENCES Mots_cles(nom),
  article_lies varchar(255),
  FOREIGN KEY (article_lies) REFERENCES Article(nom),
  statut_article varchar(255),
  FOREIGN KEY (statut_article) REFERENCES Statut_article(nom),
  comite_editorial Date NOT NULL,
  FOREIGN KEY (comite_editorial) REFERENCES Comite_editorial(reunion)
);

INSERT INTO Article (nom, date_creation, derniere_modif, nb_vue, nb_acces_via_article_lie, auteur, tag, statut_article,comite_editorial) VALUES
('COVID21', '2021-01-22', '2021-01-22 11:25:20', '10000', '2100', 'dis@sitamet.co.uk', 'santé', 'soumis','2021-05-22'),
('Coupe du monde 2022', '2021-01-15', '2021-01-15 12:22:01', '18292', '2901', 'dis@sitamet.co.uk', 'sport', 'validé','2021-05-22'),
('Report du championnat', '2021-06-25', '2021-03-15 02:02:01', '10032', '567', 'orci@vehiculaPellentesquetincidunt.net', 'sport', 'validé','2021-05-22');



--- Modérateur ---

CREATE TABLE Modérateur (
  moderateur varchar(255) PRIMARY KEY,
  FOREIGN KEY (moderateur) REFERENCES Utilisateur(mail)
);

INSERT INTO Modérateur (moderateur) VALUES
('turpis.egestas.Fusce@velitdui.org'),
('at.fringilla.purus@ornarelectusjusto.co.uk');



--- Commentaire ---
CREATE TYPE statut_comm AS ENUM ('visible', 'masqué', 'supprimé');

CREATE TABLE Commentaire (
  id INTEGER PRIMARY KEY,
  titre TEXT default NULL,
  date_ecrit varchar(255),
  texte TEXT default NULL,
  statut statut_comm,
  nb_vues integer NULL,
  taille integer NULL,
  commentaire_pere INTEGER default NULL,
  UNIQUE(titre, date_ecrit, texte, auteur),
  FOREIGN KEY (commentaire_pere) REFERENCES Commentaire(id),
  article varchar(255),
  FOREIGN KEY (article) REFERENCES Article(nom),
  auteur varchar(255),
  FOREIGN KEY (auteur) REFERENCES Lecteur(lecteur)
);

CREATE SEQUENCE seq_commentaire START 1;

INSERT INTO Commentaire (id,titre,date_ecrit,texte,nb_vues,taille,commentaire_pere,article,auteur) VALUES
(nextval('seq_commentaire'),'Article en bois','2021-05-22 11:25:03','Cet article regorge de fake news !!!',
6384,398,NULL,'COVID21', 'sed.pede.Cum@imperdiet.ca'),
(nextval('seq_commentaire'),'Re : Article en bois','2021-05-22 11:55:03','Toutes les sources sont fiables pourtant',
8384,398,1,'COVID21', 'sem.semper@condimentumeget.co.uk'),
(nextval('seq_commentaire'),'Terrifiant','2021-05-22 11:25:03','Ce nouveau virus est effrayant',
6884,38,NULL,'COVID21', 'Duis@duiFusce.edu'),
(nextval('seq_commentaire'),'IMpatience ...','2020-09-17 10:52:14','Je ne plus attendre de voir nos bleus soulever ce trophee',
677,346,NULL, 'Coupe du monde 2022','Duis@duiFusce.edu');




--- Bloc ---

CREATE TYPE type_bloc AS ENUM ('image', 'texte');

CREATE TABLE Bloc (
  id INTEGER PRIMARY KEY,
  titre varchar(255),
  typ_contenu type_bloc,
  contenu varchar(255),
  taille integer,
  article_pere varchar(255),
  FOREIGN KEY (article_pere) REFERENCES Article(nom),
  UNIQUE(titre, typ_contenu, contenu)
);

CREATE SEQUENCE seq_bloc START 1;

INSERT INTO Bloc (id, typ_contenu, titre, contenu, taille, article_pere) VALUES
(nextval('seq_bloc'),'texte', 'Paragraphe 1', 'Fluctuat nec mergitur', 200, 'Coupe du monde 2022'),
(nextval('seq_bloc'),'texte', 'Introduction', 'Est ce que la chloroquine marche vraiment?', 400, 'COVID21'),
(nextval('seq_bloc'),'image', 'Image 1', 'https://urlz.fr/cWwr', 567, 'COVID21'),
(nextval('seq_bloc'),'image', 'Image 2', 'https://urlz.fr/dkVQ', 532, 'COVID21'),
(nextval('seq_bloc'),'image', 'Image Ligue 1', 'https://urlz.fr/foot', 512, 'Report du championnat'),
(nextval('seq_bloc'),'image', 'Image Ligue 1', 'https://', 212, 'Report du championnat');
--- Rubrique ---

CREATE TABLE Rubrique (
  titre varchar(255) PRIMARY KEY,
  a_lhonneur boolean,
  nb_visites integer,
  date_creation Date NOT NULL,
  rubrique_pere varchar(255),
  FOREIGN KEY (rubrique_pere) REFERENCES Rubrique(titre)
  );

INSERT INTO Rubrique (titre, a_lhonneur, nb_visites, date_creation, rubrique_pere)  VALUES
('Santé', FALSE, 246382, '2014-12-21 21:25:23', NULL),
('Sport', TRUE, 6532, '2015-05-22 10:15:03', NULL),
('Coronavirus', TRUE, 20000, '2019-05-22 11:25:03','Santé'),
('Football', FALSE, 263911, '2015-05-22 11:25:03', 'Sport'),
('Voyage', TRUE, 2201, '2020-05-22 01:25:23', NULL);

CREATE TABLE Article_Rubrique (
  rubrique varchar(255),
  nom varchar(255),
  PRIMARY KEY (rubrique, nom),
  FOREIGN KEY (rubrique) REFERENCES Rubrique(titre),
  FOREIGN KEY (nom) REFERENCES Article(nom)
);

INSERT INTO Article_Rubrique (rubrique, nom)
VALUES
('Coronavirus','COVID21'),
('Football','Coupe du monde 2022'),
('Football','Report du championnat');

CREATE TABLE Rubrique_Index (
  mots_cles varchar(255),
  rubrique varchar(255),
  PRIMARY KEY (mots_cles, rubrique),
  FOREIGN KEY (mots_cles) REFERENCES Mots_cles(nom),
  FOREIGN KEY (rubrique) REFERENCES Rubrique(titre)
);

INSERT INTO Rubrique_Index (mots_cles, rubrique)
VALUES
('santé','Coronavirus'),
('santé', 'Santé'),
('sport','Sport'),
('sport','Football');

CREATE TABLE Commentaire_Moderateur(
  moderateur varchar(255),
  commentaire integer,
  PRIMARY KEY (moderateur, commentaire),
  FOREIGN KEY (moderateur) REFERENCES Modérateur(moderateur),
  FOREIGN KEY (commentaire) REFERENCES Commentaire(id)
);

INSERT INTO Commentaire_Moderateur (moderateur, commentaire)
VALUES
('turpis.egestas.Fusce@velitdui.org',1),
('at.fringilla.purus@ornarelectusjusto.co.uk',2),
('turpis.egestas.Fusce@velitdui.org',3),
('at.fringilla.purus@ornarelectusjusto.co.uk',3),
('turpis.egestas.Fusce@velitdui.org',4),
('at.fringilla.purus@ornarelectusjusto.co.uk',4);
--- Administrateur ---

CREATE TABLE Administrateur (
  admin varchar(255) PRIMARY KEY,
  FOREIGN KEY (admin) REFERENCES Utilisateur(mail)
);

INSERT INTO Administrateur (admin) VALUES
('vulputate@tortordictumeu.ca');

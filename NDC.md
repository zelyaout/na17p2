# Note de Clarification - Le fil électronique

## Utilisateurs

### Administrateur

## Acteurs : Administrateur

*peut supprimer et créer des comptes*

*peut supprimer et créer des comptes*

### Rédacteur

*peut créer des nouveaux articles*

*peut voir ses articles en cours de rédaction*

*peut supprimer un article*

*peut récupérer un article supprimé*

*peut soumettre son article au comité éditorial*

### Lecteur

*peut visualiser des articles*

*peut aux articles d'une rubrique/sous-rubrique*

*peut faire une recherche par mots clés*

*peut accéder à un article lié depuis un article lu*

*peut associer un commmentaire à un article*

*peut poster des commentaires*

### Éditeur

*peut voir l'ensemble des articles*

*peut accéder et modifier les articles*

*peut ajouter des mots-clés aux articles*

*peut créer des rubriques et des sous-rubriques*

*peut associer un statut à un article*

*peut faire passer une catégorie à l'honneur*

*peut lier des articles entre eux*

*choisir quels articles validés sont publiés*

*peut associer des articles à un ou plusieurs rubriques et/ou sous-rubriques*


### Modérateur

*peut masquer un commentaire*

*peut supprimer un commentaire*

*peut mettre en exergue un commentaire*

## Objets

### Utilisateurs

- nom
- prénom
- mail
- age
- rôle (administrateur, éditeur, lecteur, modérateur)

### Rubrique

- titre
- a_lhonneur
- rubrique_pere
- nom
- auteur
- statut : "à l'honneur", "normal"
- père

### Article

- auteur
- date
- statut : "validé", "en rédaction", "soumis", "en relecture", "rejeté", "supprimé"
- bloc
- mots-clés
- frère
- rubrique
- derniere_modification
- dernier_auteur

### Mots-clés

 - nom
 - auteur
 - articles

### Bloc

- titre
- contenu : texte, image

### Modification
 - type
 - auteur
 - date
### Backup

- type (article, commentaire)
- supprimé_par
- supprimé_le

### Commentaire

- titre
- texte
- article
- date
- auteur
- statut : (visible, masqué, supprimé)

## Hypothèses

H1 : Une rubrique qui a un père est une sous rubrique

H2 : Un lecteur peut poser plusieurs commentaires sous le même article

H3 : Une rubrique peut aussi être référencée par des mots-clés

H4 : Un utilisateur ne peut avoir qu'un seul rôle

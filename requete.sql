DROP VIEW IF EXISTS rubrique_a_lhonneur;
DROP VIEW IF EXISTS classement_commentaire;
DROP VIEW IF EXISTS log_trente_jour;
DROP VIEW IF EXISTS get_moderateur;


--- Selectionner les rubriques a l'honneur ---

CREATE VIEW rubrique_a_lhonneur AS
    SELECT titre
    From Rubrique as R
    WHERE a_lhonneur = TRUE;

--- Donne les commentaire racine (c'est à dire les commentaires qui n'ont pas de commentaire père)
--- par ordre de nombre de vue décroissant

CREATE VIEW classement_commentaire AS
    SELECT titre, texte, article, auteur, nb_vues
    FROM Commentaire
    WHERE commentaire_pere IS NULL
    ORDER BY nb_vues DESC;

--- Donnne les utilisateurs qui se sont connectés au cours des 30 derniers jours ---

CREATE VIEW log_trente_jour AS
    SELECT mail, nom, prenom, derniere_connexion
    FROM Utilisateur
    WHERE Date('now') - derniere_connexion  < 30
        AND Date('now') - derniere_connexion  > 0;

--- Donne les modérateurs ainsi que les commentaires qu'ils modèrent ---

CREATE VIEW get_moderateur AS
    SELECT
        CONCAT  (U.nom, ' ', U.prenom) AS "Modérateur", S.titre AS "titre de l'article", S.texte, S.auteur
    FROM Utilisateur AS U INNER JOIN Modérateur AS M ON M.moderateur = U.mail
    INNER JOIN Commentaire_Moderateur AS C ON C.moderateur = M.moderateur
    INNER JOIN Commentaire AS S ON S.id = C.commentaire;



SELECT * FROM rubrique_a_lhonneur;
SELECT * FROM classement_commentaire;
SELECT * FROM log_trente_jour;
SELECT * FROM get_moderateur;


--------------------------- Rendu supplémentaire -------------------------------

-- Affiche le nombre de commentaires pour chaque article et nombre de vues cumulées
-- des commentaires pour chaque article

SELECT a.nom, COUNT(c.id) AS nb_commentaire, SUM(c.nb_vues) AS nb_vue_commentaire
FROM Article a INNER JOIN Commentaire c ON c.article=a.nom
GROUP BY a.nom;


-- Article le plus agé pour chaque rubrique qui contient au moins 2 articles

SELECT r.titre, a.nom, a.date_creation
FROM Article a INNER JOIN Article_Rubrique ar ON a.nom=ar.nom
INNER JOIN Rubrique r ON r.titre=ar.rubrique
WHERE a.date_creation IN (SELECT MIN(b.date_creation)
  FROM Article b INNER JOIN Article_Rubrique ar ON b.nom=ar.nom
  INNER JOIN Rubrique r ON r.titre=ar.rubrique
  GROUP BY r.titre
  HAVING COUNT(b.nom)>1
);

-- Affiche le pourcentage de bloc de type image pour chaque article

SELECT a.nom,
  (SELECT COUNT(bl.id)
        FROM Bloc bl WHERE bl.typ_contenu='image' AND bl.article_pere=a.nom)::float / COUNT(b.id) * 100 AS pourcentage
FROM Bloc b INNER JOIN Article a ON b.article_pere=a.nom
GROUP BY a.nom;

-- Affiche l'age moyen des rédacteurs pour chaque rubrique

SELECT r.titre, AVG(age(NOW(),u.date_naissance))
FROM Article a INNER JOIN Article_Rubrique ar ON a.nom=ar.nom
INNER JOIN Rubrique r ON r.titre=ar.rubrique
INNER JOIN Utilisateur u ON a.auteur=u.mail
GROUP BY r.titre;

-- Requetes GROUP BY Additionnelle --

-- Affiche la moyenne de la taille moyenne des commentaires de chaque Lecteur

SELECT u.prenom, u.nom, u.mail, ROUND(AVG(c.taille),2) AS Moyenne_de_taile
FROM Utilisateur u INNER JOIN Lecteur l ON l.lecteur=u.mail
INNER JOIN Commentaire c ON c.auteur=l.lecteur
GROUP BY u.prenom, u.nom, u.mail;

-- Affiche la taille d'un article en faisant la somme de la taille des blocs qui le compose

SELECT a.nom, SUM(b.taille)
FROM Article a INNER JOIN Bloc b ON b.article_pere=a.nom
GROUP BY a.nom;
